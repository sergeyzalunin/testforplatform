﻿using Base;
using Common;
using System;
using System.Data.Entity;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        Context db;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Context.ApplyMigrations();
            this.Text = string.Format("Код базы - {0}", Global.GetCurrentBaseId());
            button_update_Click(null, null);
        }
        
        private void button_save_Click(object sender, EventArgs e)
        {
            db.Transaction(null);
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            db = new Context();

            db.BoxesTable.Load();
            db.ToysTable.Load();
            db.BoxItemsTable.Load();

            toysBindingSource.DataSource = db.ToysTable.Local.ToBindingList();
            boxesBindingSource.DataSource = db.BoxesTable.Local.ToBindingList();
            boxItemsBindingSource.DataSource = db.BoxItemsTable.Local.ToBindingList();
        }

        private void button_addToBox_Click(object sender, EventArgs e)
        {
            Toys toy = null;
            if (dataGridView1.CurrentCell.Selected)
            {
                toy = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].DataBoundItem as Toys;
            }

            Boxes box = null;
            if (dataGridView2.CurrentCell.Selected)
            {
                box = dataGridView2.Rows[dataGridView2.CurrentCell.RowIndex].DataBoundItem as Boxes;
            }

            if (toy != null && box != null)
            {
                db.BoxItemsTable.Add(new BoxItems
                {
                    Box = box,
                    Toy = toy
                });
            }
        }
    }
}
