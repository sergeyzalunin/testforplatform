﻿using NLog;
using System;
using System.Threading;

namespace Synchronizator
{
    public abstract class PeriodicTask
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly TimeSpan _timeout;
        private readonly string _taskName;

        private int _executionsCount;
        private readonly AutoResetEvent _stop;
        private RegisteredWaitHandle _registeredWait;

        protected PeriodicTask(TimeSpan timeout, string taskName)
        {
           _timeout = timeout;
            _taskName = taskName;

            _executionsCount = 0;
            _stop = new AutoResetEvent(false);
        }

        public void Start()
        {
            Logger.Info("Starting {0}...", _taskName);

            BeforeStart();
            _stop.Reset();
            _registeredWait = ThreadPool.RegisterWaitForSingleObject(_stop, Callback, null, _timeout, false);
           
            Logger.Info("{0} started", _taskName);
        }

        public void Stop()
        {
            Logger.Info("Stopping {0}...", _taskName);

            _stop.Set();
            _stop.Dispose();
            AfterStop();

            Logger.Info("{0} stopped", _taskName);
        }

        protected virtual void BeforeStart()
        {
        }

        protected virtual void AfterStop()
        {
        }

        protected abstract void Execute();

        private void Callback(object state, bool timeout)
        {
            if (timeout)
            {
                if (Interlocked.CompareExchange(ref _executionsCount, 1, 0) == 0)
                {
                    SafeExecute();
                    Interlocked.Decrement(ref _executionsCount);
                }
            }
            else
            {
               _registeredWait.Unregister(null);
               Logger.Info("{0} unregistered", _taskName);
            }
        }

        private void SafeExecute()
        {
            try
            {
                Execute();
            }
            catch (Exception e)
            {
                Logger.Error(e);
                Console.Write(e.Message);
            }
        }
    }
}
