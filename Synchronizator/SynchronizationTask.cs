﻿using Common;
using NLog;
using System;

namespace Synchronizator
{
    public class SynchronizationTask : PeriodicTask
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static object _locker = new object();
        private static TypeAction _currentAction = TypeAction.None;

        public SynchronizationTask(TimeSpan timeout, string taskName)
            : base(timeout, taskName)
        {
        }

        protected override void Execute()
        {
            lock (_locker)
            {
                if (_currentAction == TypeAction.None)
                {
                    var transport = new FileTransportProtocol();
                    if (!transport.IsSyncFileNameExists())
                    {
                        transport.CreateInitialFile();
                        _currentAction = TypeAction.Unload;
                    }
                    else
                        _currentAction = TypeAction.Load;
                }

                Worker dummy = new Worker();
                if (_currentAction == TypeAction.Unload)
                {
                    dummy.UnLoad();
                    _currentAction = TypeAction.Load;
                }
                else
                {
                    if (dummy.Load())
                        _currentAction = TypeAction.Unload;
                }

                Logger.Info("Текущее действие - "+_currentAction);
            }
        }
    }
}
