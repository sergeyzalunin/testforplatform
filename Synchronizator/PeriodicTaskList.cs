﻿using System;
using System.Linq;
using NLog;

namespace Synchronizator
{
    public class PeriodicTaskList
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly PeriodicTask[] _tasks;

        public PeriodicTaskList()
        {
            _tasks = new[]
                         {
                             Synchronization()
                         };
        }

        public void StartAll()
        {
           Logger.Info("Start sync...");
            _tasks.ToList().ForEach(t => t.Start());
        }

        public void StopAll()
        {
           Logger.Info("Start sync...");
            _tasks.ToList().ForEach(SafeStopTask);
        }

        private static void SafeStopTask(PeriodicTask task)
        {
            try
            {
                task.Stop();
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        private static PeriodicTask Synchronization()
        {
           return new SynchronizationTask(new TimeSpan(0, 0, 0, 5, 0), "Синхронизация бд");
        }
    }
}
