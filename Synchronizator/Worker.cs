﻿using Base;
using Common;
using Common.Interfaces;
using Common.Models;
using System;
using System.Globalization;
using System.Linq;

namespace Synchronizator
{
    internal class Worker : IWorker
    {
        public bool Load()
        {
            var tr = new FileTransportProtocol().Receive();
            if (tr == null)
                return false;

            var unloadTime = DateTime.UtcNow;            

            foreach (var record in tr.RecordList)
            {
                record.IsModified = false;
                if (record is BoxesModel)
                    WriteTransaction(record as BoxesModel);

                if (record is BoxItemsModel)
                    WriteTransaction(record as BoxItemsModel);

                if (record is ToysModel)
                    WriteTransaction(record as ToysModel);
            }

            return true;
        }

        public void UnLoad()
        {
            var toysTable = DependencyResolver.container.GetInstance<IModelRepository<ToysModel, Toys>>();
            var boxesTable = DependencyResolver.container.GetInstance<IModelRepository<BoxesModel, Boxes>>();
            
            var lastSyncTime = GetLastSyncTime();
            var unloadTime = DateTime.UtcNow;
            var tr = CreateTransaction(unloadTime);
            FillToysTransaction(tr, unloadTime, lastSyncTime);
            FillBoxesTransaction(tr, unloadTime, lastSyncTime);
            FillBoxItemsTransaction(tr, unloadTime, lastSyncTime);
            new FileTransportProtocol().Send(tr);
            SetLastSyncTime(unloadTime);
        }

        public DateTime GetLastSyncTime()
        {
            var settingsTable = DependencyResolver.container.GetInstance<IModelRepository<SettingsModel, Settings>>();
            var lastSyncTimeRecord = settingsTable.GetAll().FirstOrDefault(n => n.Name == "lastsynctime");
            return lastSyncTimeRecord == null ? new DateTime(1900, 1, 1) : DateTime.Parse(lastSyncTimeRecord.Value, CultureInfo.InvariantCulture);
        }

        public void SetLastSyncTime(DateTime time)
        {
            var settingsTable = DependencyResolver.container.GetInstance<IModelRepository<SettingsModel, Settings>>();
            var lastSyncTimeRecord = settingsTable.GetAll().FirstOrDefault(n => n.Name == "lastsynctime");
            if (lastSyncTimeRecord == null)
            {
                var setting = new SettingsModel
                {
                    Name = "lastsynctime",
                    Value = time.ToString(CultureInfo.InvariantCulture)
                };
                settingsTable.Write(setting);
                return;

            }
            lastSyncTimeRecord.Modified = DateTime.UtcNow;
            lastSyncTimeRecord.Value = time.ToString(CultureInfo.InvariantCulture);
            settingsTable.Write(lastSyncTimeRecord);
        }

        public Transaction CreateTransaction(DateTime actionTime)
        {
            Transaction tr = new Transaction();
            tr.ActionTime = actionTime;
            tr.BaseId = Global.GetCurrentBaseId();
            return tr;
        }

        public void FillBoxItemsTransaction(Transaction tr, DateTime unloadTime, DateTime lastSyncTime)
        {
            var boxItemsTable = DependencyResolver.container.GetInstance<IModelRepository<BoxItemsModel, BoxItems>>();
            var list = boxItemsTable.GetAll(new[] { "Box", "Toy" }).Where(n => n.Modified >= lastSyncTime && n.Modified <= unloadTime);
            foreach (var boxItems in list)
            {
                tr.RecordList.Add(boxItems);
            }
        }

        public void FillBoxesTransaction(Transaction tr, DateTime unloadTime, DateTime lastSyncTime)
        {
            var boxesTable = DependencyResolver.container.GetInstance<IModelRepository<BoxesModel, Boxes>>();
            foreach (var box in boxesTable.GetAll().Where(n => n.Modified >= lastSyncTime && n.Modified <= unloadTime))
            {
                tr.RecordList.Add(box);
            }
        }

        public void FillToysTransaction(Transaction tr, DateTime unloadTime, DateTime lastSyncTime)
        {
            var toysTable = DependencyResolver.container.GetInstance<IModelRepository<ToysModel, Toys>>();
            foreach (var toy in toysTable.GetAll().Where(n => n.Modified >= lastSyncTime && n.Modified <= unloadTime))
            {
                tr.RecordList.Add(toy);
            }
        }

        public void WriteTransaction(ToysModel record)
        {
            var toysTable = DependencyResolver.container.GetInstance<IModelRepository<ToysModel, Toys>>();
            toysTable.Write(record);
        }

        public void WriteTransaction(BoxesModel record)
        {
            var boxesTable = DependencyResolver.container.GetInstance<IModelRepository<BoxesModel, Boxes>>();
            boxesTable.Write(record);
        }

        public void WriteTransaction(BoxItemsModel record)
        {
            var boxItemsTable = DependencyResolver.container.GetInstance<IModelRepository<BoxItemsModel, BoxItems>>();
            boxItemsTable.Write(record);
        }
    }
}
