﻿using System;

namespace Synchronizator
{
    class Program
    {
        static void Main(string[] args)
        {
            DependencyResolver.Configure();
            PeriodicTaskList tasks = new PeriodicTaskList();            
            tasks.StartAll();
            WaitUserExit();
            tasks.StopAll();
        }

        private static void WaitUserExit()
        {
            Console.WriteLine("Press ENTER to Exit...");
            Console.ReadLine();
        }
    }
}
