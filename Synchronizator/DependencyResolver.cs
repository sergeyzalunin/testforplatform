﻿using Base;
using Base.Provider;
using Base.Repository;
using Common.Interfaces;
using SimpleInjector;

namespace Synchronizator
{
    internal static class DependencyResolver
    {
        public static Container container;
        
        public static void Configure()
        {            
            container = new Container();
            
            container.Register<IMapper, Mapper>();
            container.Register(typeof(IContextProvider<>), typeof(MsSqlContextProvider<>));
            container.Register(typeof(IModelRepository<,>), typeof(Repository<,>));

            container.Verify();
        }
    }
}
