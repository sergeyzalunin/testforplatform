﻿using Common;
using Common.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Synchronizator
{
    internal class FileTransportProtocol : ITransport
    {
        private string _directory = Path.Combine(Path.GetTempPath(), "TestSync");
        private string _extention = "imctpl";

        public FileTransportProtocol()
        {
            if (!Directory.Exists(_directory))
                Directory.CreateDirectory(_directory);
        }

        public string SyncFileName
        {
            get
            {
                return Path.Combine(_directory, string.Format("sync.{0}",
                                    _extention));
            }
        }

        public Transaction Receive()
        {
            Transaction tr = null;
            var baseId = Global.GetCurrentBaseId();
            var files = Directory.EnumerateFiles(_directory, string.Format("*.{0}", _extention));
            if (files.Count() == 0) return tr;

            XmlSerializer formatter = new XmlSerializer(typeof(Transaction));
            foreach(var file in files)
            {
                var isequals = false;
                using (FileStream fs = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    var tmp_tr = (Transaction)formatter.Deserialize(fs);
                    isequals = tmp_tr.BaseId != baseId;
                    if (isequals)
                    {
                        tr = tmp_tr;
                        fs.Close();
                        break;
                    }
                    fs.Close();
                }
            }

            return tr;
        }

        public void Send(Transaction tr)
        {
            using (FileStream fs = new FileStream(SyncFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
            {
                fs.SetLength(0);
                XmlSerializer formatter = new XmlSerializer(typeof(Transaction));
                formatter.Serialize(fs, tr);
                fs.Close();
            }
        }

        public void CreateInitialFile()
        {
            Transaction tr = new Transaction();
            tr.ActionTime = DateTime.UtcNow;
            tr.BaseId = Global.GetCurrentBaseId();
            if (!IsSyncFileNameExists())
                Send(tr);
        }

        public bool IsSyncFileNameExists()
        {
            var result = File.Exists(SyncFileName);
            return result;
        }
    }
}
