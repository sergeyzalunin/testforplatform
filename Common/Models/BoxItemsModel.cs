﻿using System;
using System.Xml.Serialization;

namespace Common.Models
{
    public class BoxItemsModel : BaseModel
    {
        public BoxesModel Box { get; set; }
        
        public ToysModel Toy { get; set; }
    }
}
