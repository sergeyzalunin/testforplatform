﻿using System;

namespace Common.Models
{
    public class ToysModel : BaseModel
    {
        public string Name { get; set; }
        
        public string Color { get; set; }
    }
}
