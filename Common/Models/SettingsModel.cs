﻿namespace Common.Models
{
    public class SettingsModel : BaseModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
