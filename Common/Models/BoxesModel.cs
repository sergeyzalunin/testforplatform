﻿using System;

namespace Common.Models
{
    public class BoxesModel : BaseModel
    {
        public string Number { get; set; }
                
        public string Name { get; set; }
        
        public int WidthInCm { get; set; }
        
        public int LengthInCm { get; set; }
        
        public int HeightInCm { get; set; }
    }
}
