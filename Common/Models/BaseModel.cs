﻿using System;
using System.Xml.Serialization;

namespace Common.Models
{
    [XmlInclude(typeof(BoxesModel)), XmlInclude(typeof(BoxItemsModel)), XmlInclude(typeof(ToysModel))]
    public abstract class BaseModel
    {
        public int Id { get; set; }
                
        public DateTime Modified { get; set; }

        public bool IsModified { get; set; } = true;        
    }
}
