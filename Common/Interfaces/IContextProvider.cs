﻿using System;
using System.Collections.Generic;

namespace Common.Interfaces
{
    public interface IContextProvider<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(IEnumerable<string> references);
        T GetRecord(int id);
        void Write(T record);
        void WriteAll(IEnumerable<T> records);
    }
}
