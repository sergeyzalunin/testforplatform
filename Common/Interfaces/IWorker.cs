﻿namespace Common.Interfaces
{
    public interface IWorker
    {
        bool Load();
        void UnLoad();
    }
}
