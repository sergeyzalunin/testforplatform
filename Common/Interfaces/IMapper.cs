﻿using System;

namespace Common.Interfaces
{
    public interface IMapper
    {
       TDestination Map<TSource, TDestination>(TSource source);
        object Map(object source, Type sourceType, Type destinationType);
    }
}