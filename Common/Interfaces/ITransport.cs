﻿namespace Common.Interfaces
{
    public interface ITransport
    {
        Transaction Receive();
        void Send(Transaction tr);
    }
}
