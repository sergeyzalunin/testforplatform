﻿namespace Common.Interfaces
{
    public interface IModelRepository<T, E> : IContextProvider<T> where T: class where E: class
    {

    }
}