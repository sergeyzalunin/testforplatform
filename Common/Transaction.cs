﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Common
{
    public enum TypeAction { Unload, Load, None };

    [Serializable]
    public class Transaction
    {
        public int BaseId { get; set; }
        public DateTime ActionTime { get; set; }
        public List<BaseModel> RecordList { get; set; }

        public Transaction()
        {
            RecordList = new List<BaseModel>();
        }
    }
}
