﻿using System;
using System.Configuration;

namespace Common
{
    public static class Global
    {
        public static int GetCurrentBaseId()
        {
            int value = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["baseid"], out value);
            return value;
        }
    }
}
