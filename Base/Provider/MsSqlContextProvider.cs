﻿using Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace Base.Provider
{
    public class MsSqlContextProvider<T> : IContextProvider<T>, IDisposable where T: BaseEntity
    {
        Context _context;
        
        public MsSqlContextProvider()
        {
            _context = new Context();
        }

        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
        }

        public IEnumerable<T> GetAll()
        {
            var result = _context.Set<T>().Select(n => n);
            return result;
        }

        public IEnumerable<T> GetAll(IEnumerable<string> references)
        {
            var list = this.GetAll();
            foreach(string reference in references)
                foreach(var record in list)
                    _context.Entry(record).Reference(reference).Load();
            return list;
        }

        public T GetRecord(int id)
        {
            var result = _context.Set<T>().FirstOrDefault();
            return result;
        }
        
        public void Write(T record)
        {
            if (record == null) return;

            _context.Transaction(() => 
            {
                var set = _context.Set<T>().Find(record.Id);
                if (set == null)
                {                    
                    AddOrUpdate(record, set);
                    return;
                }
                if (set.Modified < record.Modified)
                    AddOrUpdate(record, set);
            });
        }

        public void WriteAll(IEnumerable<T> records)
        {
            if (records == null) return;

            _context.Transaction(() =>
            {
                var setList = _context.Set<IEnumerable<T>>().Find(records.Select(n => n.Id));
                if (setList.Count() == 0)
                {
                    AddOrUpdate(records);
                }
                var recordsForUpdate = records.Where(n => setList.Any(m => m.Modified < n.Modified) || !setList.Contains(n)).ToList();
                AddOrUpdate(recordsForUpdate);
            });
        }

        private void AddOrUpdate(IEnumerable<T> records)
         {
            foreach(var record in records)
               _context.Set<T>().AddOrUpdate(n => n.Id, record);
         }

         private void AddOrUpdate(T record, T oldValue)
         {
            //TODO: Исправить этот хардкор. Проблема из-за отношения таблиц many-to-many
            if (record is BoxItems)
            {
                var updateValue = (oldValue ?? record) as BoxItems;

                 _context.Entry(updateValue).State = EntityState.Modified;
                 if (_context.Set<Toys>().FirstOrDefault(n => n.Id == updateValue.Toy.Id) != null)
                 {
                    _context.Entry(updateValue.Toy).State = EntityState.Modified;
                 }

                 if (_context.Set<Boxes>().FirstOrDefault(n => n.Id == updateValue.Box.Id) != null)
                 {
                     _context.Entry(updateValue.Box).State = EntityState.Modified;
                 }

            }
            _context.Set<T>().AddOrUpdate(n => n.Id, record);
            if (record.IsModified == false)
            {
                var updateValue = oldValue ?? record;
                _context.Entry(updateValue).Property(n => n.Modified).IsModified = record.IsModified;

                if (updateValue is BoxItems)
                {
                    var boxItems = (updateValue as BoxItems);
                    if (_context.Set<Toys>().FirstOrDefault(n => n.Id == boxItems.Toy.Id) != null)
                        _context.Entry(boxItems.Toy).Property(n => n.Modified).IsModified = record.IsModified;

                    if (_context.Set<Boxes>().FirstOrDefault(n => n.Id == boxItems.Box.Id) != null)
                        _context.Entry(boxItems.Box).Property(n => n.Modified).IsModified = record.IsModified;
                }

            }
         }

   }
}
