﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base
{
    [Table("BoxItems")]
    public class BoxItems : BaseEntity
    {
        [Required]
        public Boxes Box { get; set; }

        [Required]
        public Toys Toy { get; set; }        
    }
}
