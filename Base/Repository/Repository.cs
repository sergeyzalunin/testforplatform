﻿using Common.Interfaces;
using Common.Models;
using System;
using System.Collections.Generic;

namespace Base.Repository
{
    public class Repository<T, E> : IModelRepository<T, E>, IDisposable where T : BaseModel where E: class
    {
        IContextProvider<E> _provider;
        IMapper _mapper;

        public Repository(IContextProvider<E> provider, IMapper mapper)
        {
            _provider = provider;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _provider.Dispose();
        }

        public IEnumerable<T> GetAll()
        {
            IEnumerable<E> entities = _provider.GetAll();
            IEnumerable<T> result = _mapper.Map<IEnumerable<E>, IEnumerable<T>>(entities);
            return result;
        }

        public IEnumerable<T> GetAll(IEnumerable<string> references)
        {
            IEnumerable<E> entities = _provider.GetAll(references);
            IEnumerable<T> result = _mapper.Map<IEnumerable<E>, IEnumerable<T>>(entities);
            return result;
        }

        public T GetRecord(int id)
        {
            E entity = _provider.GetRecord(id);
            T result = _mapper.Map<E, T>(entity);
            return result;
        }
        
        public void Write(T record)
        {
            E model = _mapper.Map<T, E>(record);
            _provider.Write(model);
        }

        public void WriteAll(IEnumerable<T> records)
        {
            IEnumerable<E> models = _mapper.Map<IEnumerable<T>, IEnumerable<E>>(records);
            _provider.WriteAll(models);
        }
    }
}
