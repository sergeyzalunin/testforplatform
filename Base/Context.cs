﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;

namespace Base
{
    public class Context : DbContext
    {
        public DbSet<Toys> ToysTable { get; set; }
        public DbSet<Boxes> BoxesTable { get; set; }
        public DbSet<BoxItems> BoxItemsTable { get; set; }
        public DbSet<Settings> SettingsTable { get; set; }

        public Context()
        {
            this.Configuration.LazyLoadingEnabled = true;
        }
        
        public static void ApplyMigrations()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Migrations.Configuration>());

            using (Context context = new Context())
                if (!context.Database.Exists())
                    context.Database.Initialize(false);
        }
        
        public void Transaction(Action commitAction)
        {
            using (var contextTransaction = this.Database.BeginTransaction())
                try
                {
                    commitAction?.Invoke();
                    this.SaveChanges();
                    contextTransaction.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            contextTransaction.Rollback();
                            throw ex;
                        }
                    }
                }
                catch (Exception ex)
                {
                    contextTransaction.Rollback();
                    throw ex;
                }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var objectStateEntries = ChangeTracker.Entries()
                .Where(e => e.Entity is BaseEntity 
                         && (e.State == EntityState.Modified 
                         ||  e.State == EntityState.Added
                         ||  e.State == EntityState.Deleted)).ToList();

            var currentTime = DateTime.UtcNow;
            foreach (var entry in objectStateEntries)
            {
                var entityBase = entry.Entity as BaseEntity;
                if (entityBase == null) continue;

                if (entityBase.Id == 0)
                   entityBase.Id = GetLasID(entry);

                if (entityBase.IsModified)
                    entityBase.Modified = currentTime;
            }

            return base.SaveChanges();
        }

        public int GetLasID(DbEntityEntry entry)
        {
            string tableName = entry.Entity.GetType().Name;
            int result = GetLasID(tableName);
                        
            return result;
        }

        public int GetLasID(string tableName)
        {
            int result = 0;

            var value = GetSettingsLastRecord(tableName);
            if (value == null)
            {
                value = new Settings
                {
                    Id = tableName == "Settings" ? Int32.Parse(ConfigurationManager.AppSettings["seed"]) : GetLasID("Settings"),
                    Modified = DateTime.UtcNow,
                    Name = tableName,
                    Value = ConfigurationManager.AppSettings["seed"]
                };
                SettingsTable.Add(value);
            }
            result = Int32.Parse(value.Value) + 1;
            value.Value = result.ToString();

            return result;
        }

        public IQueryable<T> GetData<T>(DbEntityEntry entry) where T : class
        {
            var method = typeof(DbContext).GetMethod("Set", new Type[0]).MakeGenericMethod(entry.Entity.GetType());
            IQueryable<T> genericItem = (IQueryable<T>)method.Invoke(this, new object[0]);
            return genericItem;
        }

        public Settings GetSettingsLastRecord(string tableName)
        {
            var value = this.SettingsTable.FirstOrDefault(n => n.Name == tableName);
            var changes = this.ChangeTracker.Entries().FirstOrDefault(n =>
                                                n.Entity is Settings
                                            && (n.Entity as Settings).Name == tableName);

            if (changes == null || value == null)
                value = value ?? (changes == null ? null : changes.Entity as Settings);
            else
                value = new[] { value, (changes.Entity as Settings) }.OrderByDescending(n => n.Modified).FirstOrDefault();
            return value;
        }
    }
}
