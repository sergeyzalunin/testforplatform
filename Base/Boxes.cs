﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base
{
    [Table("Boxes")]
    public class Boxes : BaseEntity
    {
        [StringLength(50), Column(TypeName = "nvarchar"), Required]
        public string Number { get; set; }

        [StringLength(50), Column(TypeName = "nvarchar"), Required]
        public string Name { get; set; }

        [Required]
        public int WidthInCm { get; set; }

        [Required]
        public int LengthInCm { get; set; }

        [Required]
        public int HeightInCm { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
