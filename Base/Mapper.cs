﻿using Common.Interfaces;
using Common.Models;
using System;

namespace Base
{
    public class Mapper : IMapper
    {
        static Mapper()
        {
            AutoMapper.Mapper.Initialize(InitializeMapping);
            AutoMapper.Mapper.AssertConfigurationIsValid();
        }

        static void InitializeMapping(AutoMapper.IMapperConfigurationExpression config)
        {
            config.CreateMap<Toys, ToysModel>().ReverseMap();
            config.CreateMap<Boxes, BoxesModel>().ReverseMap();
            config.CreateMap<BoxItems, BoxItemsModel>().ReverseMap();
            config.CreateMap<Settings, SettingsModel>().ReverseMap();
        }
        public object Map(object source, Type sourceType, Type destinationType)
        {
            return AutoMapper.Mapper.Map(source, sourceType, destinationType);
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return AutoMapper.Mapper.Map<TSource, TDestination>(source);
        }
    }
}
