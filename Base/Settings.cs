﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
    public class Settings : BaseEntity
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
