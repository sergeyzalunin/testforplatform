﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base
{
    [Table("Toys")]
    public partial class Toys : BaseEntity
    {
        [StringLength(50), Column(TypeName = "nvarchar"), Required]
        public string Name { get; set; }

        [StringLength(50), Column(TypeName = "nvarchar"), Required]
        public string Color { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
    
}
