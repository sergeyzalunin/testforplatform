﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Base
{
    public abstract class BaseEntity
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None), ConcurrencyCheck]
        public int Id { get; set; }

        [Required]
        public DateTime Modified { get; set; }

        [NotMapped]
        public bool IsModified { get; set; } = true;
    }
}
